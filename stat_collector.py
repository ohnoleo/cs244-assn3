from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink, Intf
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from mininet.util import quietRun
from mininet.node import Node, OVSKernelSwitch

from argparse import ArgumentParser

import re
import sys
import os
import math
import numpy
import time
import Skype4Py
import subprocess
from signal import SIGINT

CELLSIM_PATH = "cellsim/sender/cellsim"
SPROUT_PATH  = "alfalfa/src/examples/sproutbt2"

SAMPLE_RATE = 1

THROUGHPUT_IFACE = "cell-eth1"

parser = ArgumentParser(description="Collects statistics for peer-to-peer client over lossy link")
parser.add_argument('--up-trace', '-u',
                    dest="uplink_trace",
                    action="store",
                    help="Path to PPS network trace for CellSim uplink",
                    required=True)
parser.add_argument('--dn-trace', '-d',
                    dest="downlink_trace",
                    action="store",
                    help="Path to PPS network trace for CellSim downlink",
                    required=True)
parser.add_argument('--loss-ratio',
                    dest="loss_ratio",
                    type=float,
                    help="Ratio of packets, between 0 and 1, that get randomly dropped by CellSim",
                    default=0.0)
parser.add_argument('--client', '-c',
                    dest="client",
                    action="store",
                    help="Client to test",
                    choices=["cli","skype","sprout"],
                    default="cli")
parser.add_argument('--out-dir',
                    dest="root_out_dir",
                    action="store",
                    help="Path to store output",
                    required=True)
parser.add_argument('--cell-scalar',
                    dest="cell_scalar",
                    action="store",
                    type=int,
                    help="CellSim scalar value in bytes",
                    default=1500)
parser.add_argument('--duration',
                    dest="duration",
                    action="store",
                    type=int,
                    help="Duration, in seconds, to run the test",
                    default=300)

args = parser.parse_args()
out_dir = args.root_out_dir + ("/s%i/" % args.cell_scalar)
d = os.path.dirname(out_dir)

if not os.path.exists(d):
    os.makedirs(d)

test_info = open(out_dir+"test_info","w")
test_info.write("uplink=%s\n" % args.uplink_trace)
test_info.write("dnlink=%s\n" % args.downlink_trace)
test_info.write("cell_scalar=%i\n" % args.cell_scalar)
test_info.close()

#################################

def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))
    # Extract TX bytes from:
    # Inter-|   Receive                                                |  Transmit
    # face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    # lo: 6175728   53444    0    0    0     0          0         0  6175728   53444    0    0    0     0       0          0
    return float(line.split()[9])

def get_rates(iface, nsamples, period):
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time.time()
        elapsed = now - last_time
        #if last_time:
        #    print "elapsed: %0.4f" % (now - last_time)
        last_time = now
        # Get rate in kbps; correct for elapsed time.
        rate = (txbytes - last_txbytes) * 8.0 / 1e3 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append([now, rate])
        last_txbytes = txbytes
        print '.',
        sys.stdout.flush()
        time.sleep(period)
    return ret

#################################

def write_throughput (throughput, out_dir):
    out_file = open(out_dir+"/"+args.client+"_throughput","w")
    for entry in throughput:
        out_file.write ("%f\t%f\n" % (entry[0],entry[1]))
    out_file.close()

#################################

def initCellSim (cell_node, host_node, uplink_trace, downlink_trace, loss_ratio):
    cell_node.cmd("ifconfig %s up promisc" % cell_node.intfs[0],shell=True)
    cell_node.cmd("ifconfig %s up promisc" % cell_node.intfs[1],shell=True)

    swapCellSim (cell_node, host_node, uplink_trace, downlink_trace, loss_ratio)


def swapCellSim (cell_node, host_node, uplink_trace, downlink_trace, loss_ratio):
    internal_mac = host_node.MAC(host_node.intfs[0])
    timestamp = time.time()
    cell_node.cmd("killall cellsim")
    cell_node.cmd("nohup %s %s %s %s %f %s %s %i >%s/cellsim-stdout-%s 2>%s/cellsim-stderr-%s &" % 
               (CELLSIM_PATH, uplink_trace, downlink_trace, internal_mac, loss_ratio, cell_node.intfs[1], cell_node.intfs[0], args.cell_scalar, out_dir, args.client, out_dir, args.client),
               shell=True)

def startNAT (inetIntf, root):
    """Start NAT/forwarding between Mininet and external network
    inetIntf: interface for internet access
    root: node to access iptables from"""

    # Identify the interface connecting to the mininet network
    localIntf =  root.intfs[0]
    
    # Flush any currently active rules
    root.cmd( 'iptables -F' )
    root.cmd( 'iptables -t nat -F' )
    
    # Create default entries for unmatched traffic
    root.cmd( 'iptables -P INPUT ACCEPT' )
    root.cmd( 'iptables -P OUTPUT ACCEPT' )
    root.cmd( 'iptables -P FORWARD DROP' )
    
    # Configure NAT
    root.cmd( 'iptables -I FORWARD -i ' + localIntf.name + ' -d 10.0.0.0/255.255.255.0 -j DROP' )
    root.cmd( 'iptables -A FORWARD -i ' + localIntf.name + ' -s 10.0.0.0/255.255.255.0 -j ACCEPT' )
    root.cmd( 'iptables -A FORWARD -i ' + inetIntf + ' -d 10.0.0.0/255.255.255.0 -j ACCEPT' )
    root.cmd( 'iptables -t nat -A POSTROUTING -o ' + inetIntf + ' -j MASQUERADE' )
    
    # Instruct the kernel to perform forwarding
    root.cmd( 'sysctl net.ipv4.ip_forward=1' )

def stopNAT (root):
    """Stop NAT/forwarding between Mininet and external network"""
    # Flush any currently active rules
    root.cmd( 'iptables -F' )
    root.cmd( 'iptables -t nat -F' )

    # Instruct the kernel to stop forwarding
    root.cmd( 'sysctl net.ipv4.ip_forward=0' )

def connectToInternet (network):
    "Connect the network to the internet"
    switch = network.switches[ 0 ]  # switch to use
    ip = '10.0.0.254'  # our IP address on host network
    routes = [ '10.0.0.0/24' ]  # host networks to route to
    prefixLen = 24 # subnet mask length
    inetIface = "eth0" # host interface for internet connectivity

    # Create a node in root namespace and link to switch 0
    root = Node( 'root', inNamespace=False )
    intf = network.addLink (root, switch)
    root.setIP( ip, prefixLen )

    # Start network that now includes link to root namespace
    network.start()

    # Start NAT and establish forwarding
    startNAT( inetIface, root )

    # Establish routes from end hosts
    for host in network.hosts:
        host.cmd( 'ip route flush root 0/0' )
        host.cmd( 'route add -net 10.0.0.0/24 dev ' + host.intfs[0].name )
        host.cmd( 'route add default gw ' + ip )

    # TODO: sometimes root-eth0 does not get brought up for some reason.
    #       This can be fixed with 'ifconfig root-eth0 10.0.0.254 up' but
    #       if we do this after skype launches it could take a while to
    #       sync back up. Try to detect this case and fix it before we
    #       start everything up, or better yet, figure out why it happens
    #       in the first place.
    gw_status = subprocess.check_output("ifconfig root-eth0", shell=True)
    if gw_status.find("error") != -1:
        subprocess.call("ifconfig root-eth0 %s up" % ip, shell=True)

    # Start cellsim up
    cell = network.getNodeByName("cell")
    h1 = network.getNodeByName("h1")
    h2 = network.getNodeByName("h2")
    initCellSim (cell, h1,
                 "init.pps", "init.pps",
                 0.0)

    if args.client=="cli":
        swapCellSim (cell, h1,
                     args.uplink_trace, args.downlink_trace,
                     args.loss_ratio)

        print
        print "*** Hosts are running and should have internet connectivity"
        print "*** Type 'exit' or control-D to shut down network"
        CLI (network)
    elif args.client=="sprout":

        time.sleep(5)

        swapCellSim (cell, h1,
                     args.uplink_trace, args.downlink_trace,
                     args.loss_ratio)

        print 'Starting sprout instances...'
        h1.popen('%s > %s/sprout_h1.out 2>&1' % (SPROUT_PATH, out_dir), shell=True)
        h2.popen('%s %s 60001 > %s/sprout_h2.out 2>&1' % (SPROUT_PATH, h1.IP(), out_dir), shell=True)

        throughput = get_rates(THROUGHPUT_IFACE, float(args.duration)/SAMPLE_RATE, SAMPLE_RATE)
        write_throughput (throughput, out_dir)

        subprocess.call("sudo killall -9 sproutbt2", shell=True)
        
    elif args.client=="skype":
	print "Spawning test video streamer"
        video_stream = subprocess.Popen ("gst-launch-1.0 videotestsrc ! v4l2sink device=/dev/video0", shell=True)

        print "Initializing Skype clients"

	if os.path.exists("./skype_dbs/SkypeA"):
            subprocess.call("rm -rf ./skype_dbs/SkypeA", shell=True)
        if os.path.exists("./skype_dbs/SkypeB"):
            subprocess.call("rm -rf ./skype_dbs/SkypeB", shell=True)
        subprocess.call("cp -a ./skype_dbs/SkypeA_template ./skype_dbs/SkypeA", shell=True);
        subprocess.call("cp -a ./skype_dbs/SkypeB_template ./skype_dbs/SkypeB", shell=True);
        h1.cmd("python skype_client.py start SkA ./skype_dbs/SkypeA", shell=True)
        h2.cmd("python skype_client.py start SkB ./skype_dbs/SkypeB", shell=True)

	time.sleep(1)

        h2.popen("python skype_client.py wait_for_call SkB cs244_user_a", shell=True)
        time.sleep(.5)
        h1.cmd("python skype_client.py call SkA cs244_user_b", shell=True)
        time.sleep(.5)
        h1.cmd("python skype_client.py enable_video SkA", shell=True)
        h2.cmd("python skype_client.py enable_video SkB", shell=True)

        time.sleep(5)

        swapCellSim (cell, h1,
                     args.uplink_trace, args.downlink_trace,
                     args.loss_ratio)

        throughput = get_rates(THROUGHPUT_IFACE, float(args.duration)/SAMPLE_RATE, SAMPLE_RATE)

        #subprocess.call("sudo killall -9 tcpdump", shell=True)

        h1.cmd("python skype_client.py hangup SkA", shell=True)
        h2.cmd("python skype_client.py hangup SkB", shell=True)
        h1.cmd("python skype_client.py stop SkA", shell=True)
        h2.cmd("python skype_client.py stop SkB", shell=True)

        write_throughput (throughput, out_dir)

        # gstreamer is silly and doesn't respond to SIGTERM or SIGINT very
        # gracefully, sigh
        subprocess.call("sudo killall -9 gst-launch-1.0", shell=True)

    stopNAT (root)
    network.stop()

class CellTopo(Topo):
    "Simple topology for simulating capacity-bursty links."

    def __init__(self):
        super(CellTopo, self).__init__()

        h1 = self.addHost('h1')
        cell = self.addHost('cell')
        h2 = self.addHost('h2')

        # All links in this experiment are 1 gbps
        s0 = self.addSwitch('s0', bw=1000)

        self.addLink(h1, cell, bw=1000, delay="1ms")
        self.addLink(cell, s0, bw=1000, delay="1ms")
        self.addLink(h2, s0, bw=1000, delay="2ms")

        return

def run_experiment():
    topo = CellTopo()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    connectToInternet(net)

if __name__ == "__main__":
    run_experiment()