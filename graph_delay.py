from argparse import ArgumentParser
import pickle
import matplotlib.pyplot as plt
import matplotlib

def unpack(delays):
  times = []
  values = []
  for delay in delays:
    times.append(delay[0])
    values.append(delay[1])
  return times, values

def graph(args):
  skype_delay_file = args.dir+"/skype_delays.out"
  sprout_delay_file = args.dir+"/sprout_delays.out"

  skype_delay = pickle.load(open(skype_delay_file, 'rb'))
  sprout_delay = pickle.load(open(sprout_delay_file, 'rb'))

  plt.yscale('log')
  plt.ylim([20, 1000])
  plt.xlabel('time (s)')
  plt.ylabel('Per-packet delay (ms)')

  skype_delay = unpack(skype_delay)
  plt.plot(*skype_delay, alpha=.7, label='Skype', color='red')
  plt.plot(*unpack(sprout_delay), alpha=.7, label='Sprout', color='blue')

  plt.xlim([0, max(skype_delay[0])])

  plt.hlines(100, 0, max(skype_delay[0]), linestyles='dotted')
  plt.legend()

  plt.title("Per-packet delay, %s byte events" % test_metadata["cell_scalar"])

  DefaultSize = plt.gcf().get_size_inches()
  plt.gcf().set_size_inches( (DefaultSize[0]*2, DefaultSize[1]) )

  if args.out != None:
    plt.savefig(args.out)
  else:
    plt.show()

parser = ArgumentParser(description='Delay graph script.')
parser.add_argument('dir', help='Test directory')
parser.add_argument('out', help='Output file', default=None)
args = parser.parse_args()

test_metadata = {}
test_info_file = open(args.dir+"/test_info","r")
for line in test_info_file:
    if line[-1] == '\n':
        line = line[:-1]
    tokens = line.split("=",1)
    test_metadata[tokens[0]]=tokens[1]
test_info_file.close()

graph(args)
