from argparse import ArgumentParser
import matplotlib.pyplot as plt
import numpy

parser = ArgumentParser()
parser.add_argument('-o', '--out',
                    help="Save plot to output file, e.g.: --out plot.png",
                    dest="out",
                    default=None)
parser.add_argument('--dir',
                    dest="dir",
                    help="Directory from which outputs of the sweep are read.",
                    required=True)
parser.add_argument('-r', '--resolution',
                    help="Resolution, in seconds, of capacity trace",
                    type=int,
                    dest="resolution",
                    default=1)

args = parser.parse_args()

##############################################################
# Parse test metadata
##############################################################

test_metadata = {}
test_info_file = open(args.dir+"/test_info","r")
for line in test_info_file:
    if line[-1] == '\n':
        line = line[:-1]
    tokens = line.split("=",1)
    test_metadata[tokens[0]]=tokens[1]
test_info_file.close()

##############################################################
# Parse throughput
##############################################################

def parse_throughput(filename): 
    keys = []
    values = []
    base_time = -1

    throughput_file = open(filename,"r")
    for line in throughput_file:
        tokens = line.split("\t")
        if base_time == -1:
            base_time = float(tokens[0])
        keys.append((float(tokens[0])-base_time))
        values.append(float(tokens[1].replace("\n","")))
    throughput_file.close()
    return (keys, values)

(skype_keys, skype_throughput) = parse_throughput(args.dir+"/skype_throughput")
(sprout_keys, sprout_throughput) = parse_throughput(args.dir+"/sprout_throughput")

##############################################################
# Parse PPS trace 
##############################################################

trace_file = open(test_metadata["uplink"],"r")

trace_arrivals = trace_file.read()
trace_file.close()
trace_arrivals = trace_arrivals.split("\n")
if trace_arrivals[-1]=="":
    trace_arrivals.pop(-1)
trace_arrivals = map(lambda x: float(x)/1e3, trace_arrivals)

capacity = []
capacity_keys = numpy.arange(0,max(sprout_keys),args.resolution)

last_index = 0
key_index = 0
while key_index < len(capacity_keys):
    key_value = capacity_keys[key_index]
    start_index = last_index
    end_index = len(trace_arrivals)-1

    while abs(start_index-end_index)>1:
        search_index = ((end_index - start_index)/2)+start_index

        if trace_arrivals[search_index] < key_value:
            start_index = search_index
        else:
            end_index = search_index

    while trace_arrivals[end_index] == key_value and \
          end_index+1 < len(trace_arrivals):
        end_index += 1

    period = float(capacity_keys[key_index] - capacity_keys[key_index-1])
    capacity.append((end_index - last_index)*(8*float(test_metadata["cell_scalar"])/1024)/period)

    last_index = end_index
    key_index += 1

##############################################################

plt.plot(skype_keys, skype_throughput, alpha=.7, lw=2, label="Skype", color="red")
plt.plot(sprout_keys, sprout_throughput, alpha=.7, lw=2, label="Sprout", color="blue")
plt.plot(capacity_keys[0:len(capacity)], capacity, ls="--", lw=2, label="Capacity", color="black")

plt.yscale('log')

plt.xlim((min(sprout_keys), max(sprout_keys)))
plt.ylim((1e1, max(capacity)+1e4))
plt.legend(loc='best')
plt.ylabel("Throughput (Kbps)")
plt.xlabel("Time (s)")

plt.title("Throughput, %s byte events" % test_metadata["cell_scalar"])

DefaultSize = plt.gcf().get_size_inches()
plt.gcf().set_size_inches( (DefaultSize[0]*2, DefaultSize[1]) )

if args.out != None:
    plt.savefig(args.out)
else:
    plt.show()
