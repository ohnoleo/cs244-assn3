#!/usr/bin/python
import sys
import re

i = 0
f_in = open(sys.argv[1],"r")
f_out = open(sys.argv[1]+".pps","w")

base_time = int(re.findall(r'\d+',f_in.readline())[0])
f_out.write("%i\n" % 0);

for line in f_in:
   time = int(re.findall(r'\d+',line)[0])
   rel_time = time-base_time
   f_out.write("%i\n" % (rel_time / 1e6));

f_in.close()
f_out.close()
