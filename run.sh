#!/bin/bash

ROOT_UID="0"

#Check if run as root
if [ "$UID" -ne "$ROOT_UID" ] ; then
	echo "You must be root!"
	exit 1
fi

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT

exptid=`date +%b%d-%H:%M`
rootdir=alfalfa-out-$exptid

python ./stat_collector.py -u verizon4g-uplink.rx.pps -d verizon4g-downlink.rx.pps --client skype --out-dir $rootdir --cell-scalar 1500 --duration 300
python ./stat_collector.py -u verizon4g-uplink.rx.pps -d verizon4g-downlink.rx.pps --client sprout --out-dir $rootdir --cell-scalar 1500 --duration 300

python ./stat_collector.py -u verizon4g-uplink.rx.pps -d verizon4g-downlink.rx.pps --client skype --out-dir $rootdir --cell-scalar 100 --duration 300
python ./stat_collector.py -u verizon4g-uplink.rx.pps -d verizon4g-downlink.rx.pps --client sprout --out-dir $rootdir --cell-scalar 100 --duration 300

python ./gen_graph.py --dir $rootdir/s100 --out $rootdir/s100_throughput.png
python ./gen_graph.py --dir $rootdir/s1500 --out $rootdir/s1500_throughput.png

python parse_cellsim.py $rootdir/s100/cellsim-stderr-sprout > $rootdir/s100/sprout_delays.out
python parse_cellsim.py $rootdir/s100/cellsim-stderr-skype > $rootdir/s100/skype_delays.out
python graph_delay.py $rootdir/s100 $rootdir/s100_delay.png

python parse_cellsim.py $rootdir/s1500/cellsim-stderr-sprout > $rootdir/s1500/sprout_delays.out
python parse_cellsim.py $rootdir/s100/cellsim-stderr-skype > $rootdir/s1500/skype_delays.out
python graph_delay.py $rootdir/s1500 $rootdir/s1500_delay.png
