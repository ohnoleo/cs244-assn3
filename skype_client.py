#!/usr/bin/env python

# TODO: without modifying skype4py we can do this:
#import os
#os.environ["DBUS_SESSION_BUS_ADDRESS"] = conn_str

import sys
import time
import Skype4Py
import subprocess

hold = True

def launchSkype(dbus_id, dbstring):
    subprocess.Popen("dbus-launch --exit-with-session ./dbus_skype_init.sh %s skype --dbpath=%s" % (dbus_id, dbstring) , shell=True)
    time.sleep (1)

def attach(dbus_id):
    dbus_output = open("dbus_conn_str_%s" % dbus_id , "r")
    dbus_conn_str = dbus_output.read().replace("\n","")
    dbus_output.close()

    print ("Opening DBUS connection")
    SkypeClient = Skype4Py.Skype(UseCustomBus=dbus_conn_str)
    SkypeClient.FriendlyName = 'CS244_Alfalfa_Experiment'

    sys.stdout.write("Attaching to Skype")
    while True:
        try:
            SkypeClient.Attach(100)
            break
        except Skype4Py.errors.SkypeAPIError:
            sys.stdout.write('.')
            sys.stdout.flush()
            time.sleep(1.0)
    sys.stdout.write("\n")

    return SkypeClient

def shutdown(dbus_id, SkypeClient):
    SkypeClient.Client.Shutdown()
    grep_dbus_id = "["+dbus_id[0]+"]"+dbus_id[1:]
    dbus_pids = subprocess.check_output("ps aux | grep %s | awk '{print $2}'" % grep_dbus_id, shell=True)
    dbus_pids = dbus_pids.replace("\n", " ")
    subprocess.Popen("kill %s" % dbus_pids, shell=True)
    subprocess.Popen("rm dbus_conn_str_%s" % dbus_id, shell=True)

def waitForCall(call, status):
    global hold
    try:
        if status == Skype4Py.clsRinging:
            if call.PartnerHandle == sys.argv[3]:
                hold = False
                call.Answer()
            else:
                call.Finish()
    except:
        pass

###################################################

argc = len(sys.argv)
if argc < 2:
    print("Command required")
    exit(1)

command = sys.argv[1].lower()
if command == "start" :
    if argc < 4:
        print("Usage: start instance_id skype_db_path")
        exit(1)
    launchSkype(sys.argv[2], sys.argv[3])
elif command == "print_user" :
    if argc < 3:
        print("Usage: print_user instance_id")
        exit(1)
    client = attach(sys.argv[2])
    print client.CurrentUserHandle
elif command == "stop" :
    if argc < 3:
        print("Usage: stop instance_id")
        exit(1)
    client = attach(sys.argv[2])
    shutdown(sys.argv[2], client)
elif command == "call" :
    if argc < 4:
        print("Usage: call instance_id username")
        exit(1)
    client = attach(sys.argv[2])
    client.PlaceCall(sys.argv[3])
elif command == "enable_video" :
    if argc < 3:
        print("Usage: enable_video instance_id")
        exit(1)
    client = attach(sys.argv[2])
    
    calls = client.ActiveCalls
    if len(calls) > 0 :
        for call in calls:
            call.StartVideoSend()
            call.StartVideoReceive()
    else:
        print("No active calls")
elif command == "hangup":
    if argc < 3:
        print("Usage: hangup instance_id [username]")
        exit(1)
    client = attach(sys.argv[2])

    calls = client.ActiveCalls
    if len(calls) > 0 :
        for call in calls:
            if (argc > 3) :
                if call.PartnerHandle == sys.argv[3]:
                    call.Finish()
            else:
                call.Finish()                    
    else:
        print("No active calls")
elif command == "wait_for_call" :
    if argc < 4:
        print("Usage: wait_for_call instance_id username")
        exit(1)
    client = attach(sys.argv[2])
    client.OnCallStatus = waitForCall
    while hold:
	time.sleep(0.25)
else :
    print("Command not recognized")

time.sleep(2)