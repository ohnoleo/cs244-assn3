#include <unistd.h>
#include <string>
#include <assert.h>
#include <list>
#include <time.h>
#include <sys/time.h>


#include "sproutconn.h"
#include "select.h"

using namespace std;
using namespace Network;

void print_time() {
  struct timeval tv;
  struct tm *tm;

  gettimeofday(&tv, NULL);
  tm=localtime(&tv.tv_sec);

  long seconds = (((long)tm->tm_hour * 60 + tm->tm_min) * 60) + tm->tm_sec;
  long usecs = seconds * 1000 + tv.tv_usec / 1000;
  fprintf(stderr, "%ld: ", usecs);
}

int main( int argc, char *argv[] )
{
  char *ip;
  int port;

  Network::SproutConnection *net;

  bool server = true;

  if ( argc > 1 ) {
    /* client */

    server = false;

    ip = argv[ 1 ];
    port = atoi( argv[ 2 ] );

    fprintf(stderr, "Connecting...\n");
    net = new Network::SproutConnection( "4h/Td1v//4jkYhqhLGgegw", ip, port );
  } else {
    fprintf(stderr, "Starting server...\n");
    net = new Network::SproutConnection( NULL, NULL );
  }

  fprintf( stderr, "Port bound is %d\n", net->port() );

  Select &sel = Select::get_instance();
  sel.add_fd( net->fd() );

  const int fallback_interval = 50;

  /* wait to get attached */
  if ( server ) {
    while ( 1 ) {
      int active_fds = sel.select( -1 );
      if ( active_fds < 0 ) {
	perror( "select" );
	exit( 1 );
      }

      if ( sel.read( net->fd() ) ) {
	net->recv();
      }

      if ( net->get_has_remote_addr() ) {
	break;
      }
    }
  }

  uint64_t time_of_next_transmission = timestamp() + fallback_interval;

  fprintf( stderr, "Looping...\n" );  

  /* loop */
  int seqno = 0;
  while ( 1 ) {
    int bytes_to_send = net->window_size();

    /* actually send, maybe */
    if ( ( bytes_to_send > 0 ) || ( time_of_next_transmission <= timestamp() ) ) {
      do {
	int this_packet_size = std::min( 1440, bytes_to_send );
	bytes_to_send -= this_packet_size;
	assert( bytes_to_send >= 0 );

	string garbage( this_packet_size, 'x' );
        memcpy(&garbage[0], &seqno, 4);
        seqno++;

	int time_to_next = 0;
	if ( bytes_to_send == 0 ) {
	  time_to_next = fallback_interval;
	}

        print_time();
        fprintf(stderr, "Sending some stuff (seqno %d)...\n", seqno);
	net->send( garbage, time_to_next );
      } while ( bytes_to_send > 0 );

      time_of_next_transmission = std::max( timestamp() + fallback_interval,
					    time_of_next_transmission );
    }

    /* wait */
    int wait_time = time_of_next_transmission - timestamp();
    if ( wait_time < 0 ) {
      wait_time = 0;
    } else if ( wait_time > 10 ) {
      wait_time = 10;
    }

    int active_fds = sel.select( wait_time );
    if ( active_fds < 0 ) {
      perror( "select" );
      exit( 1 );
    }

    /* receive */
    if ( sel.read( net->fd() ) ) {
      string packet( net->recv() );
      int seqno;
      memcpy(&seqno, packet.c_str(), 4);
      print_time();
      fprintf(stderr, "I got a packet (seqno %d)!\n", seqno);
    }
  }
}
