import re
import sys
import datetime
import time
from collections import namedtuple
from argparse import ArgumentParser
import pickle

Packet = namedtuple('Packet', ('time', 'seqno', 'line'))

def unix_time(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()


def unix_time_millis(dt):
    return unix_time(dt) * 1000.0


def parse_dump(filename, match_receiver, sprout_mode, is_sender):
  if sprout_mode:
    return parse_sprout_dump(filename, match_receiver, is_sender)
  else:
    return parse_tcp_dump(filename, match_receiver, is_sender)


def parse_sprout_dump(filename, match_receiver, is_sender):
  lines = open(filename).readlines()
  packets = []
  for line in lines:
    if match_receiver is not None and match_receiver not in line:
      continue
    if is_sender and 'Sending some stuff' not in line:
      continue
    if not is_sender and 'got a packet' not in line:
      continue

    match = re.match('(.*):.*seqno ([0-9]+)', line)
    if not match:
      continue
    seqno = match.group(2)
    packets.append(Packet(float(match.group(1)) / 1000, int(seqno), line))

  return packets


def parse_tcp_dump(filename, match_receiver, is_sender):
  lines = open(filename).readlines()
  packets = []
  for line in lines:
    if match_receiver is not None and match_receiver + ':' not in line:
      continue
    match = re.match('(.*) IP .*seq ([0-9]+:)?([0-9]+)', line)
    if not match:
      continue
    date = datetime.datetime.strptime(match.group(1), '%Y-%m-%d %H:%M:%S.%f')
    seqno = match.group(3)
    packets.append(Packet(unix_time_millis(date), int(seqno), line))

  return packets


def match(args):
  sender_dump = parse_dump(args.sender_dump, args.receiver_info, args.sprout, is_sender=True)
  receiver_dump = parse_dump(args.receiver_dump, args.receiver_info, args.sprout, is_sender=False)

  start_time = sender_dump[0].time

  delays = []
  for send_packet in sender_dump:
    for recv_packet in receiver_dump:
      if recv_packet.seqno != send_packet.seqno:
        continue
      if abs(recv_packet.time - send_packet.time) > 5:
        continue

      # Found a match.
      delay = recv_packet.time - send_packet.time
      delays.append((send_packet.time - start_time, delay * 1000))
      break

  print pickle.dumps(delays)


parser = ArgumentParser(description="Dump matching script.")
parser.add_argument('sender_dump', help="Sender's dump")
parser.add_argument('receiver_dump', help="Receiver's dump")
parser.add_argument('--receiver-info', '-i', dest='receiver_info',
  help="IP + port tuple to select for. Example: 127.0.0.1.80")
parser.add_argument('--sprout', action='store_true', help="Enable Sprout mode. Otherwise, enable Skype mode.")

match(parser.parse_args())
