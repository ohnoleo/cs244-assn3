import sys
import pickle

lines = open(sys.argv[1]).readlines()

delays = []
for line in lines:
  if 'downlink' not in line:
    continue
  if 'delivery' not in line:
    continue
  chunks = line.split(' ')
  delays.append((float(chunks[1]), int(chunks[3])))

print pickle.dumps(delays)
