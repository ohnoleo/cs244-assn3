#!/usr/bin/env python

# TODO: without modifying skype4py we can do this:
#import os
#os.environ["DBUS_SESSION_BUS_ADDRESS"] = conn_str

import sys
import time
import Skype4Py
import subprocess

###########################################################
# Launch Skype A
###########################################################

subprocess.Popen("dbus-launch --exit-with-session ./dbus_skype_init.sh ska skype --dbpath=~/.SkypeUserA", shell=True)

time.sleep (1)

dbus_output = open("dbus_conn_str_ska","r")
dbus_conn_str = dbus_output.read().replace("\n","")
dbus_output.close()

print ("Opening DBUS connection")
SkypeA = Skype4Py.Skype(UseCustomBus=dbus_conn_str)

SkypeA.FriendlyName = 'CS244_Alfalfa_Experiment'

sys.stdout.write("Attaching to Skype")
while True:
    try:
        SkypeA.Attach(100)
        break
    except Skype4Py.errors.SkypeAPIError:
	sys.stdout.write('.')
	sys.stdout.flush()
        time.sleep(1.0)
sys.stdout.write("\n")

###########################################################
# Launch Skype B
###########################################################

subprocess.Popen("dbus-launch --exit-with-session ./dbus_skype_init.sh skb skype --dbpath=~/.SkypeUserB", shell=True)

time.sleep (1)

dbus_output = open("dbus_conn_str_skb","r")
dbus_conn_str = dbus_output.read().replace("\n","")
dbus_output.close()

SkypeB = Skype4Py.Skype(UseCustomBus=dbus_conn_str)
SkypeB.FriendlyName = 'CS244_Alfalfa_Experiment'

sys.stdout.write("Attaching to Skype")
while True:
    try:
        SkypeB.Attach()
        break
    except Skype4Py.errors.SkypeAPIError:
        sys.stdout.write('.')
        time.sleep(1)
sys.stdout.write("\n")

###########################################################
# Do whatever we want
###########################################################

print SkypeA.CurrentUserHandle
print SkypeB.CurrentUserHandle


###########################################################
# Clean up after ourselves
###########################################################

SkypeA.Client.Shutdown()
SkypeB.Client.Shutdown()

dbus_pids = subprocess.check_output("ps | grep [d]bus-launch | awk '{print $1}'", shell=True)
dbus_pids = dbus_pids.replace("\n", " ")
subprocess.Popen("kill %s" % dbus_pids, shell=True)
subprocess.Popen("rm dbus_conn_str_ska", shell=True)
subprocess.Popen("rm dbus_conn_str_skb", shell=True)
